---
template: home
blocks:
  -
    type: hero
    photo: /assets/mountains-norway.jpg
    content: |
      # Homepage
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper, velit ut malesuada dictum, dui enim imperdiet justo, non bibendum quam leo vel felis. Suspendisse in nibh massa.
    title: Homepage
    hero_options:
      - overlay
    background_image: /assets/mountains-norway.jpg
    overlay: true
    bg_img: /assets/mountains-norway.jpg
    height: hero-tall
  -
    type: entry
    content: 'Hello, this is the homepage content. [Link to modal test](#modal)'
title: Home
fieldset: flexible
id: 9e26590f-bead-4a9d-a1bc-4d068da2863e
---
Hello, this is the homepage content [Link](http://#0)