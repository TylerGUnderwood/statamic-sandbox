// import './src/modal.js';

function modal() {

    // element for opening the modal
    var openable = document.getElementById('modal' + modalId);
    // modal overlay
    var overlay = document.getElementById('modal_overlay' + modalId);
    // the modal
    var modal = document.getElementById('modal' + modalId);
    // modal close button
    var closeButton = document.getElementById('modal_close' + modalId);
    // element for closing the modal
    var closeable = document.querySelector('[data-modal="close"]' + modalId);

    // for each modal button activate its corresponding modal on click
    openable.each.addEventListener("click", function() {
        event.preventDefault();

        modal.classList.toggle('modal-closed');
        overlay.classList.toggle('modal-closed');
        closeButton.classList.toggle('modal-closed');
    });

    // for each button that closes the modal on click, close the modal
    closeable.each.addEventListener("click", function() {
        event.preventDefault();

        modal.classList.toggle('modal-closed');
        overlay.classList.toggle('modal-closed');
        closeButton.classList.toggle('modal-closed');
    });

};

modal();